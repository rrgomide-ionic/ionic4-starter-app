export interface Tarefa {
  id: string;
  title: string;
  description: string;
}
