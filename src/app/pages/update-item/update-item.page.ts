import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup
} from '@angular/forms';

import { TarefaService } from './../../services/tarefa.service';
import { Tarefa } from './../../interfaces/Tarefa';

@Component({
  selector: 'update-item',
  templateUrl: './update-item.page.html',
  styleUrls: ['./update-item.page.scss']
})
export class UpdateItemPage implements OnInit {
  item: Tarefa;
  editItemForm: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    private tarefaService: TarefaService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(param => {
      const id = param.id;

      this.item = this.tarefaService.getItem(id);

      if (!this.item) {
        return;
      }

      this.editItemForm = this.formBuilder.group({
        title: new FormControl(this.item.title, Validators.required),
        description: new FormControl(this.item.description, Validators.required)
      });
    });
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  updateItem(value) {
    const newValues = {
      id: this.item.id,
      title: value.title,
      description: value.description
    };

    this.tarefaService.updateItem(newValues);
    this.goBack();
  }
}
