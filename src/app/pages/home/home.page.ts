import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TarefaService } from './../../services/tarefa.service';
@Component({
  selector: 'app-page-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  /**
   * Construtor
   * @param router Serviço de roteamento, que substituio navParams
   * @param tarefaService Serviço de CRUD de tarefas
   */
  constructor(private router: Router, public tarefaService: TarefaService) {}

  openNewItemPage() {
    this.router.navigate(['/new-item']);
  }

  goToItem(idTarefa: string) {
    this.router.navigate(['/update-item', idTarefa]);
  }
}
