import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup
} from '@angular/forms';

import { TarefaService } from './../../services/tarefa.service';

@Component({
  selector: 'new-item',
  templateUrl: './new-item.page.html',
  styleUrls: ['./new-item.page.scss']
})
export class NewItemPage implements OnInit {
  newItemForm: FormGroup;

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private tarefaService: TarefaService
  ) {}

  ngOnInit() {
    this.newItemForm = this.formBuilder.group({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  createItem(value) {
    this.tarefaService.createItem(value.title, value.description);
    this.newItemForm.reset();
    this.goBack();
  }
}
