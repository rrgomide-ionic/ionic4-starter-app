import { Injectable } from '@angular/core';

import { Tarefa } from './../interfaces/Tarefa';
@Injectable({
  providedIn: 'root'
})
export class TarefaService {
  items: Array<Tarefa> = [
    {
      id: '1',
      title: 'Estudar',
      description: 'Estudar PMH.'
    },
    {
      id: '2',
      title: 'Exercícios',
      description: 'Exercícios IGTI.'
    },
    {
      id: '3',
      title: 'Atividade Prévia',
      description: 'Cumprir atividade prévia.'
    },
    {
      id: '4',
      title: 'Correr',
      description: 'Correr 3x por semana.'
    }
  ];

  constructor() {}

  createItem(title, description) {
    const randomId = Math.random()
      .toString(36)
      .substr(2, 5);

    this.items.push({
      id: randomId,
      title: title,
      description: description
    });

    console.log(this.items);
  }

  getItems(): Tarefa[] {
    return [].concat(this.items);
  }

  getItem(id: string): Tarefa {
    const items = this.getItems();
    return items.find((item: Tarefa) => item.id === id);
  }

  updateItem(newValues) {
    const itemIndex = this.items.findIndex(item => item.id === newValues.id);
    this.items[itemIndex] = newValues;
  }
}
